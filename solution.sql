--find all artist id with letter d 
SELECT * FROM artists WHERE name LIKE "%d%";

--find all songs with less than 230
SELECT * FROM songs WHERE length < 230;

--join album and songs table 
SELECT songs.song_name, albums.album_title, songs.length FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- join artist and albums table that has letter a in it
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
WHERE name LIKE "%a%";

--sort album to z-a but limit to 4
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- join albums and songs table
SELECT *  FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id
	ORDER BY album_title DESC; 